Package.describe({
  summary: 'Templates and view-models for bootstrap components.',
  version: '0.0.1',
  git: 'https://gitlab.com/pje/bootstrap-templates.git'
});

Package.onUse(function (api) {
  api.versionsFrom('1.2.0.2');

  api.use([
    'coffeescript'
  ], 'client');

  api.addFiles([
    // Core.
    'src/core/viewmodel.coffee',
    'src/core/component.coffee',

    // Dropdown.
    'src/dropdown/dropdown.coffee',
    'src/dropdown/dropdown.html',
    'src/dropdown-link/dropdown-link.coffee',
    'src/dropdown-link/dropdown-link.html',
    'src/dropdown-header/dropdown-header.coffee',
    'src/dropdown-header/dropdown-header.html',
    'src/dropdown-divider/dropdown-divider.coffee',
    'src/dropdown-divider/dropdown-divider.html'
  ], 'client');
});

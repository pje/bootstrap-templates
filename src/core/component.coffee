window.twbs.Component = Component

class Component extends twbs.ViewModel
  constructor: (@template, @id, @components = []) ->
    check @id, (Match.Optional String)
    check @components, [Component]
    super @template

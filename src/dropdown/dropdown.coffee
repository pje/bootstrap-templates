window.twbs.Dropdown = Dropdown

class Dropdown extends twbs.Component
  constructor: (@id, @components, @label, @buttonId = @id + '-button', @up = false) ->
    check @label, String
    check @buttonId, String
    check @up, Boolean
    super 'dropdown', @id, @components

  direction: -> if @up then 'up' else 'down'

window.twbs.Dropdown.Link = Link

class Link extends twbs.Component
  constructor: (@id, @href, @text, @disabled = false) ->
    check @href, String
    check @text, String
    check @disabled, Boolean
    super 'dropdown-link', @id

  disabled: -> if @disabled then 'disabled' else null

window.twbs.Dropdown.Header = Header

class Header extends twbs.Component
  constructor: (@id, @text) ->
    check @text, String
    super 'dropdown-header', @id
